def OvenFun():
    raw=input('What do you have to put in?\n')
    if raw=='bread':
        cooked='toast'
    elif raw=='meat':
        cooked='roast'
    elif raw=='dough':
        cooked='cookies'
    else:
        cooked='disappointment'
    print("In goes the %s, out comes the %s" % (raw, cooked))
#    return cooked

OvenFun()
